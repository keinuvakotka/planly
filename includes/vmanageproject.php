<?php $project=getProject($db, $id);?>
<h1><span class="small">Manage: </span> <?php echo $project->name;?></h1>
<div class="clear">
<form action="http://www.planly.eu/manage/<?php echo $project->admincode ?>" method="post">
<input type="hidden" name="mode" value="edit">	
<input type="submit" class="clear button" value="Edit project data" class="button">
</form>
<?php 
if($project->rating == 'false'){
	echo '<form action="http://www.planly.eu/manage/'.$project->admincode.'" method="post">';
	echo '<input type="hidden" name="mode" value="start-rating">';
	echo '<input type="submit" class="clear button" value="Start project rating" class="button">';
	echo '</form>';
} else {
	echo '<form action="http://www.planly.eu/pick-solution/'.$project->admincode.'" method="post">';
	echo '<input type="hidden" name="mode" value="start-picking">';
	echo '<input type="submit" class="clear button" value="Close project & choose solution" class="button">';
	echo '</form>';
}
?>

<form action="http://www.planly.eu/" method="post">
<input type="hidden" name="mode" value="delete">
<input type="hidden" name="id" value="<?php echo $project->Id ?>">	
<input type="submit" class="clear button" value="Delete project" class="button">
</form>
</div>
<div class="left-clear">
<h1><span class="small">Invite more people sharing this link:</span></h1>
<p class="mediumSizeMe">http://www.planly.eu/contribute/<?php echo $project->code;?></p>
</div>
