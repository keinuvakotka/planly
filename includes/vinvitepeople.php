<form action="<?php echo SITE_ROOT ?>thank-you" method="post">
	<h1><span class="small">Invite people to solve:</span><?php echo $project?></h1>
	
	<img src="<?php echo SITE_ROOT?>assets/img/enterrecipients.png" width="300" height="50" class="help-inline" id="helperEmail">
	<input type="text" onkeyup="handleChangeEmail()" id="email" name="email" class="megaSizeMe" placeholder="Email to">
	
	<input type="hidden" name="problem" value="<?php echo $project?>">
	
	<input type="submit" value="Send" class="button">
</form>
<?php echo visualiseProgress(3, 4)?>
