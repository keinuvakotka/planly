<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Planly | Projects made easy</title>
<link href="<?php echo SITE_ROOT?>assets/css/app.css" rel="stylesheet" type="text/css">
<link href="<?php echo SITE_ROOT?>assets/css/rome.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
<div id="container">
	<div id="content">
		<div id="main">