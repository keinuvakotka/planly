<form action="<?php echo SITE_ROOT?>save-project" method="post">
	<h1><span class="small">Solving:</span> <?php echo $_SESSION['problem'];?></h1>
	
	<img src="<?php echo SITE_ROOT?>assets/img/solutiondl.png" width="300" height="50" class="help-inline" id="helperSolutionDL">
	<input type="text" onkeyup="handleChangeSolutionDL()" id="solutionDL" name="solutionDL" class="megaSizeMe" placeholder="Solution DL">
	
	<img src="<?php echo SITE_ROOT?>assets/img/ratingdl.png" width="300" height="50" class="help-inline" id="helperRatingDL">
	<input type="text" onkeyup="handleChangeRatingDL()" id="ratingDL" name="ratingDL" class="megaSizeMe" placeholder="Rating DL">
	
	<img src="<?php echo SITE_ROOT?>assets/img/enteryouremail.png" width="150" height="50" class="help-inline" id="helperOwnEmail">
	<input type="text" onkeyup="handleChangeOwnEmail()" id="user_email" name="user_email" class="megaSizeMe" placeholder="Your email address">
	
	<input type="hidden" name="problem" value="<?php echo $_SESSION['problem'];?>">
	
	<input type="submit" value="Send" class="button">
</form>
<?php echo visualiseProgress(2, 4)?>

