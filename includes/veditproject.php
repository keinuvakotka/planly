<?php $project=getProject($db, $id);?>
<form action="http://www.planly.eu/manage/<?php echo $project->admincode ?>" method="post">
	<h1><span class="small">Editing:</span> <?php echo $project->name;?></h1>
	
	<img src="<?php echo SITE_ROOT?>assets/img/starthere.png" width="151" height="50" class="help-inline" id="helper">
	<input name="problem" id="problem" onkeyup="handleChange()" type="text" maxlength="25" class="megaSizeMe" placeholder="<?php echo $project->name;?>">
	
	<img src="<?php echo SITE_ROOT?>assets/img/solutiondl.png" width="300" height="50" class="help-inline" id="helperSolutionDL">
	<input type="date" onkeyup="handleChangeSolutionDL()" id="solutionDL" name="solutionDL" class="megaSizeMe" placeholder="<?php echo $project->solutionDL;?>">
	
	<img src="<?php echo SITE_ROOT?>assets/img/ratingdl.png" width="300" height="50" class="help-inline" id="helperRatingDL">
	<input type="date" onkeyup="handleChangeRatingDL()" id="ratingDL" name="ratingDL" class="megaSizeMe" placeholder="<?php echo $project->ratingDL;?>">
	
	<input type="hidden" name="edit" value="true">
	
	<input type="submit" value="Update" class="button">
</form>

