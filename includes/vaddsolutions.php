<?php
$project = getProject($db, $id);
?>

<h1>
	<span class="small">Solving: </span><?php echo($project->name)?></h1>
<p><span class="medium">Time remaining</span></p>
<div id="initial"><?php echo($project->solutionDL)?></div>
<div id="clockdiv">
	<div>
		<span class="days"></span>
		<div class="smalltext">Days</div>
	</div>
	<div>
		<span class="hours"></span>
		<div class="smalltext">Hours</div>
	</div>
	<div>
		<span class="minutes"></span>
		<div class="smalltext">Minutes</div>
	</div>
	<div>
		<span class="seconds"></span>
		<div class="smalltext">Seconds</div>
	</div>
</div>
<img src="<?php echo SITE_ROOT?>assets/img/starthere.png" width="151" height="50"class="help-inline">
<form method="post" action="<?php echo SITE_ROOT?>save-input">
	<div id="solutions">
		<div id="1" class="left-clear">
			<input class="solution-form" name="solution" type="text"
				placeholder="Your idea">
		</div>
	</div>

	<div class="left">
		<input type="hidden" name="id" value="<?php echo $project->Id; ?>">
		<input type="hidden" name="rating" value="false">
		<button type="button" class="button" onclick="addSolution()">Add another</button>
		<input type="submit" class="button" name="done-brainstorming"
			value="Done!">
	</div>
</form>
<?php echo visualiseProgress(1, 2)?>