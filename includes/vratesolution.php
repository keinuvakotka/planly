<?php
require_once("functions/functions.php");
require_once("config/config.php");
$solutions = getSolutions($db, $id);
$radioid = 0;
$groupid = 0;
$project=getProject($db, $id);
?>
<h1><span class="small">Rating: </span><?php echo($project->name)?></h1>
<p><span class="medium">Time remaining</span></p>
<div id="initial"><?php echo($project->ratingDL)?></div>
<div id="clockdiv">
	<div>
		<span class="days"></span>
		<div class="smalltext">Days</div>
	</div>
	<div>
		<span class="hours"></span>
		<div class="smalltext">Hours</div>
	</div>
	<div>
		<span class="minutes"></span>
		<div class="smalltext">Minutes</div>
	</div>
	<div>
		<span class="seconds"></span>
		<div class="smalltext">Seconds</div>
	</div>
</div>

	<?php 
	if($project->closed != 'true'){
		echo'<img src="'.SITE_ROOT.'assets/img/ratethese.png" width="150" height="50"class="help-inline">';
		echo '<form action="'.SITE_ROOT.'save-input" method="post">';
		foreach ($solutions as $solutionName){
	
	?>
        
        <div class="left-clear">
           <p align="left" name="rating<?php echo $groupid; ?>" class="left solution-form"><?php echo $solutionName->solution; ?></p>
            
            <input value="minus" type="radio" id="radio<?php echo $radioid; ?>" name="<?php echo $solutionName->solution; ?>" />
            <label for="radio<?php echo $radioid; ?>">Don't like</label>
             
            <?php $radioid++; ?>
            
            <input value="zero" type="radio" id="radio<?php echo $radioid; ?>" name="<?php echo $solutionName->solution; ?>" checked />
            <label for="radio<?php echo $radioid; ?>">Ok</label>
             
            <?php $radioid++; ?>
            
            <input value="plus" type="radio" id="radio<?php echo $radioid; ?>" name="<?php echo $solutionName->solution; ?>" />
            <label for="radio<?php echo $radioid; ?>">Like</label>
            <?php $groupid++; ?>
       		<?php $radioid++; ?>
        </div>
	<?php }
			echo '<input type="hidden" name="id" value="'. $project->Id .'">';
			echo '<input type="hidden" name="rating" value="true">';
			echo '<input type="submit" class="button" name="<?php echo $id;?>" value="Done!">';
			echo '</form>';
	 } else {
		echo '<div class="left-clear"><p class="mediumSizeMe">Rating has closed, thank you for your input.</p></div>';
	} ?>
<?php echo visualiseProgress(2, 2)?>