<?php
session_set_cookie_params ( 0 );
session_start ();
require_once ("config/config.php");
require_once ("functions/functions.php");

// Header
include ("includes/vheader.php");

if(isset($_GET['email'])){
	$email = $_GET['email'];
	if (strrpos($email,'@') !== false) {
		echo '<p class="megaSizeMe">'.$email.'<p>';
		echo '<p class="mediumSizeMe">removed from list<p>';
		unSubscribe($db, $email);
	} else {
		echo '<p class="megaSizeMe">Something went wrong.<p>';
	}
} else {
	echo '<p class="megaSizeMe">Something went wrong.<p>';
}

include ("includes/vfooter.php");
?>