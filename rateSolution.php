<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

if(isset($_POST['code'])){
	$code = $_POST['code'];
} else {
	header('Location: /planly');
}

$id = checkCode($db, $code);

//Header
include("includes/vheader.php");



if(!$id){
	echo '<p class="megaSizeMe">Not a valid code<p>';
	echo '<a href="#" onclick="showDialog()" class="code focus">Try again?</a>';
}else {
	include("includes/vratesolution.php");
}

include("includes/vfooter.php");
?>



