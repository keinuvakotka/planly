// *Magic*

var SITE_ROOT = 'http://www.planly.eu/';

var helper = document.getElementById('helper');
var helperCode = document.getElementById('helperCode');
var helperContact = document.getElementById('helperContact');
var helperOwnEmail = document.getElementById('helperOwnEmail');
var helperEmail = document.getElementById('helperEmail');
var helperSolutionDL = document.getElementById('helperSolutionDL');
var helperRatingDL = document.getElementById('helperRatingDL');
var modalContainer = document.getElementById('modalContainer');
var modalDialog = document.getElementById('modalDialog');
var xhr = new XMLHttpRequest();

var buttons;
var buttonsCount;
var solutions = [];
var tasks = [];

function handleChange() {
	if(document.getElementById('problem').value.length > 2){
		helper.src= SITE_ROOT + "assets/img/pressenter.png";
	} else {
		helper.src= SITE_ROOT + "assets/img/starthere.png";
	}
}

function handleChangeCode() {
	if(document.getElementById('code').value.length > 2){
		helperCode.src= SITE_ROOT + "assets/img/pressenter50px.png";
	} else {
		helperCode.src= SITE_ROOT + "assets/img/codehere.png";
	}
}

function handleChangeContact() {
	if(document.getElementById('contact').value.length > 2){
		helperContact.src= SITE_ROOT + "assets/img/done.png";
	} else {
		helperContact.src= SITE_ROOT + "assets/img/writeyourname.png";
	}
}

function handleChangeOwnEmail() {
	if(document.getElementById('user_email').value.length > 2){
		helperOwnEmail.src= SITE_ROOT + "assets/img/done.png";
	} else {
		helperOwnEmail.src= SITE_ROOT + "assets/img/enteryouremail.png";
	}
}

function handleChangeEmail() {
	if(document.getElementById('email').value.length > 2){
		helperEmail.src= SITE_ROOT + "assets/img/done300px.png";
	} else {
		helperEmail.src= SITE_ROOT + "assets/img/enterrecipients.png";
	}
}

function handleChangeSolutionDL() {
	if(document.getElementById('solutionDL').value.length > 2){
		helperSolutionDL.src= SITE_ROOT + "assets/img/done300px.png";
	} else {
		helperSolutionDL.src= SITE_ROOT + "assets/img/solutiondl.png";
	}
}

function handleChangeRatingDL() {
	if(document.getElementById('ratingDL').value.length > 2){
		helperRatingDL.src= SITE_ROOT + "assets/img/done300px.png";
	} else {
		helperRatingDL.src= SITE_ROOT + "assets/img/ratingdl.png";
	}
}

function addRow(){
	// Gets count of solutions
	var count = document.getElementById('solutions').childElementCount;
	count = parseInt(count)+1;
	// Add one solution
	var html = '<div id="' + count + '" class="left-clear"><input class="solution-form" name="solution" type="text" placeholder="Solution"></div>';
	document.getElementById('solutions').innerHTML += html;
}

function deleteElement(id){
	var remove = solutions[id];
	// Remove solution from solutions[] & draw rows again 
	for(var i = 0; i<= solutions.length; i++){
		if(solutions[i]===remove){
			// Splice array where indicated & return updated array (from .splice() function)
			solutions.splice(i,1);
		}
	}
	updateRows(solutions);
}

function updateRows(solutions){
	// Function draws solutions as rows
	document.getElementById('solutions').innerHTML="";
	for(var i=0; i<solutions.length; i++){
		// input readonly to prevent modification of given data
		var html = '<div id="' + i + '" class="left-clear"><input readonly class="solution-form" name="solution'+ i +'" type="text" value="'+solutions[i]+'"><button class="delete-solution-form" onclick="deleteElement('+i+')">X</button></div>';
		document.getElementById('solutions').innerHTML += html;
	}
	
	// Add solution placeholder for user input
	var html = '<div id="' + i + '" class="left-clear"><input class="solution-form" name="solution'+ i +'" type="text" placeholder="Your idea"></div>';
	document.getElementById('solutions').innerHTML += html;
	//updateButtons();
}

function addSolution(){
	// Add single solution to solutions[]
	var handle = document.getElementsByClassName('solution-form');
	// Select last solution = newest
	var solution = handle.length-1;
	if(handle[solution].value.length > 2){
		solutions.push(handle[solution].value);
		updateRows(solutions);
	}
}

function countPoints(){
	var solutionCount = document.getElementsByClassName('solution');
	var solutionArray = [];
	
	for(var i = 1; i <= solutionCount.length; i++){
		
		var data = new Object();
		
		  var names = document.getElementById('group'+i+'').textContent;	
		  var radios = document.getElementsByName('group'+i+'');
		  var value;
		  
		  for(var h = 0; h < radios.length ; h++){
			  
			  if(radios[h].checked === true){
				  value = parseInt(radios[h].value);
			  }
			  
		  }
		  data.name = names;
		  data.value = value;
		  solutionArray.push(data);
		
	}
	
	var str_json = "json="+JSON.stringify(solutionArray);
	
	xhr.open("POST", SITE_ROOT);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	   
		xhr.onreadystatechange = function(){
			if(xhr.readyState == 4 && xhr.status == 200 ) {
				//window.location.assign("test.php")
				console.log(xhr.responseText);
			}
		}
		xhr.send(str_json);
}

function showDialog(){
	// Animation with css-class & css transitions
	modalContainer.className = "visible";
	document.getElementById('code').focus();
}
function hideDialog(){
	modalContainer.className = "hidden";
}
function toggle(event){
	// prevent div click through
    event.preventDefault();
    event.stopPropagation();
}

/*
 * Code handling timers for Solution adding/rating
 */

function getTimeRemaining(endtime){
	  var t = Date.parse(endtime) - Date.parse(new Date());
	  var seconds = Math.floor( (t/1000) % 60 );
	  var minutes = Math.floor( (t/1000/60) % 60 );
	  var hours = Math.floor( (t/(1000*60*60)) % 24 );
	  var days = Math.floor( t/(1000*60*60*24) );
	  return {
	    'total': t,
	    'days': days,
	    'hours': hours,
	    'minutes': minutes,
	    'seconds': seconds
	  };
	}

	function initializeClock(id, endtime){
	  var clock = document.getElementById(id);
	  var daysSpan = clock.querySelector('.days');
	  var hoursSpan = clock.querySelector('.hours');
	  var minutesSpan = clock.querySelector('.minutes');
	  var secondsSpan = clock.querySelector('.seconds');

	  function updateClock(){
	    var t = getTimeRemaining(endtime);

	    daysSpan.innerHTML = t.days;
	    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
	    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
	    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

	    if(t.total<=0){
	      clearInterval(timeinterval);
	    }
	  }

	  updateClock();
	  var timeinterval = setInterval(updateClock,1000);
	}

	//var deadline = 'December 31 2015';
	var deadline = document.getElementById('initial');
	if(deadline){
		initializeClock('clockdiv', deadline.innerHTML);
		deadline.innerHTML = '';
	}
	

/*
 * Code handling ROME external lib for date-time pickers 
 */

if(document.getElementById('solutionDL')){
	
	var moment = rome.moment;

	rome(solutionDL, {
	  time: true,
	  weekdayFormat: 'short',
	  weekStart: 1,
	  timeInterval: 3600,
	  min: moment(),
	  inputFormat: 'DD MMMM YYYY HH:mm',
	  dateValidator: rome.val.beforeEq(ratingDL)
	});

	rome(ratingDL, {
	  time: true,
	  weekdayFormat: 'short',
	  weekStart: 1,
	  timeInterval: 3600,
	  inputFormat: 'DD MMMM YYYY HH:mm',
	  dateValidator: rome.val.afterEq(solutionDL)
	});
}

function updateTaskRows(tasks){
	// Function draws solutions as rows
	document.getElementById('tasks').innerHTML="";
	for(var i=0; i<tasks.length; i++){
		// input readonly to prevent modification of given data
		var html = '<div id="' + i + '" class="left-clear work-division task"><input readonly style="width:50%;" class="solution-form" name="task' + i + '" type="text" value="'+tasks[i]+'"><input readonly style="width:50%;" class="solution-form" name="worker' + i + '" type="text" value="'+tasks[i]+'"><button class="delete-solution-form" onclick="deleteElement('+i+')">X</button></div>';
		// var html = '<div id="' + i + '" class="left-clear"><input readonly class="solution-form" name="solution'+ i +'" type="text" value="'+solutions[i]+'"><button class="delete-solution-form" onclick="deleteElement('+i+')">X</button></div>';
		document.getElementById('tasks').innerHTML += html;
	}
	
	// Add solution placeholder for user input
	var html = '<div id="' + i + '" class="left-clear work-division task"><input style="width:50%;" class="solution-form" name="task' + i + '" type="text" placeholder="Task to divide"><input style="width:50%;" class="solution-form" name="worker' + i + '" type="text" placeholder="Who is going to do it?"></div>';
	document.getElementById('tasks').innerHTML += html;
	//updateButtons();
}

function addTask(){
	// Add single solution to solutions[]
	var handle = document.getElementsByClassName('solution-form');
	// Select last solution = newest
	var task = handle.length-1;
	if(handle[task].value.length > 2){
		tasks.push(handle[task].value);
		updateTaskRows(tasks);
	}
}