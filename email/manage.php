<?php

$problem = 'Title';
$email = 'my.email@mail.com';
$solutionDL = 'Today';
$ratingDL = 'Tomorrow';
$code = 'p0t4t035';
$adminCode = 'p0t4t035-yeah!';

$html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;">';
$html.='<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;">';
$html.='<tr><td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e"><tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">Manage your project</h1>';
$html.='</td></tr></table><!-- End Header--></td></tr><tr><td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600"><tr><td valign="top" style="background-color: #fdfdfd;"><!-- Content -->';
$html.='<table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '.$email.', you'."'".'ve created project<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'.$problem.'</h2>';
$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Follow this link to project management</h3><p><a href="http://www.planly.eu/manage/'.$adminCode.'" style="color:#d6336e;"><strong>planly.eu/manage/'.$adminCode.'</strong></a></p>';
$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Or provide your own feedback here</h3><p><a href="http://www.planly.eu/contribute/'.$code.'" style="color:#d6336e;"><strong>planly.eu/contribute/'.$code.'</strong></a></p><table cellspacing="0" cellpadding="0" style="width: 100%;vertical-align: top;" border="0">';
$html.='<tr><td valign="top"><h3 style="color:#505050; display:block; font-family:Arial;font-size:20px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Project timing</h3><p>Solutions by '.$solutionDL.'<br>Ratings by '.$ratingDL.'</p></td>';
$html.='</tr></table></div></td></tr></table><!--End Content --></td></tr></table><!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer --><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p></td></tr></table>';
$html.='</td></tr></table><!-- End Footer --></td></tr></table><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Don'."'".'t want these messages anymore? <strong><a href="http://www.planly.eu/unsubscribe/'.$email.'" style="color:#9495a4">unsubscribe</a></strong></p>';
$html.='</td></tr></table></td></tr></table></td></tr></table></div></body></html>';

echo($html);
?>