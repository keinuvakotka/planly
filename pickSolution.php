<?php
session_set_cookie_params (0);
session_start();
require_once("functions/functions.php");
require_once("config/config.php");
$radioid = 0;

if(isset($_GET['code'])){
	$code = $_GET['code'];
} else {
	header('Location: /planly');
}

if(isset($_POST['mode']) && $_POST['mode'] == 'start-picking'){
	$id = getProjectID($db, $code, true);
	closeProject($db, $id);
}

//Header
include("includes/vheader.php");

$id = getProjectID($db, $code, true);

if(isset($id)){
	$_SESSION['id'] = $id;
	$project=getProject($db, $id);
	if($project->closed == 'true'){
		$solutions = getSolutions($db, $id);
		include("includes/vpicksolution.php");
	} else {
		echo '<p class="mediumSizeMe">Project deadlines not met. Please close project in <a href="http://www.planly.eu/manage/'.$project->admincode.'" style="color:#fff; font-weight:400;">management</a> or wait for it to close.<p>';
	}
} else {
	echo '<p class="mediumSizeMe">Something went wrong, please try again.<p>';
}

//Footer
include("includes/vfooter.php");
?>



