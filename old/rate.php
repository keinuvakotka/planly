<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Planly | Pre-planning Tool For Projcts</title>
<link href="assets/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="logo"><img src="assets/img/planly-logo-iso.png" width="200" height="100" alt="logo"></div>
<div id="centrify">
<form action="delegate.php">
  <p>
      <input type="text" placeholder="Solution 1">
      <input type="submit" value="Like">
      <input type="submit" value="Dislike" class="secondary">
  </p>
  <p>
      <input type="text" placeholder="Solution 2">
      <input type="submit" value="Like">
      <input type="submit" value="Dislike" class="secondary">
  </p>
</form>
</div>
</body>
</html>