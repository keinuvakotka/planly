<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Planly | Pre-planning Tool For Projcts</title>
<link href="assets/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="logo"><img src="assets/img/planly-logo-iso.png" width="200" height="100" alt="logo"></div>
<div id="centrify">
<form action="">
	<p>
		<input type="text" placeholder="Task 1">
		<input type="submit" value="Assign">
	</p>
    <p>
        <input type="text" placeholder="Task 2">
        <input type="submit" value="Assign">
    </p>
</form>
</div>
</body>
</html>