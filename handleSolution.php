<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

if(isset($_POST['code'])){
	$code = $_POST['code'];
} else {
	header('Location: http://www.planly.eu');
}

$id = getProjectID($db, $code, false);

$project=getProject($db, $id);
if(isset($project)){
	if($project->rating == 'true'){
		$rating = true;
	} else {
		$rating = false;
	}
	
	$_SESSION['id'] = $id;
	$_SESSION['rating'] = $rating;

}

//Header
include("includes/vheader.php");



if(!$id){
	echo '<p class="megaSizeMe">Not a valid code<p>';
	echo '<a href="#" onclick="showDialog()" class="code focus">Try again?</a>';
}else {
	if($rating){
		include("includes/vratesolution.php");
	} else {
		include("includes/vaddsolutions.php");
	}
}

include("includes/vfooter.php");
?>