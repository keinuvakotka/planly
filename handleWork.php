<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

if(isset($_POST['code'])){
	$code = $_POST['code'];
} else {
	header('Location: /planly');
}

$id = getProjectID($db, $code);

$project=getProject($db, $id);
if(is_object($project)){
	if($project->delegate == 'true'){
		$delegate = true;
	} else {
		$delegate = false;
	}
	
	$_SESSION['id'] = $id;
	$_SESSION['delegate'] = $delegate;

}

//Header
include("includes/vheader.php");



if(!$id){
	echo '<p class="megaSizeMe">Not a valid code<p>';
	echo '<a href="#" onclick="showDialog()" class="code focus">Try again?</a>';
}else {
	if($delegate){
		include("includes/vworkdivision.php");
	} else {
		include("includes/vpicksolutions.php");
	}
}

include("includes/vfooter.php");
?>