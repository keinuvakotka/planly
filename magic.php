<?php
date_default_timezone_set('Europe/Helsinki');

$projects = getAllProjects($db);

foreach ($projects as $project){
	$solutionDL = strtotime($project->solutionDL);
	$ratingDL = strtotime($project->ratingDL);
	$id = $project->Id;

	$today = strtotime(date("d F Y H:i"));
	
	if($today > $solutionDL && $project->rating == 'false'){
		startRating($db, $id);
	}
	
	if($today > $ratingDL && $project->closed == 'false'){
		closeProject($db, $id);
	}
	
}

?>