<?php 
/**
 * progressbar.php creates an universally usable visualisation
 * to describe users position in a defined process,
 * requires a) position in b) contained set
 */
visualiseProgress(2, 4);
function visualiseProgress($position, $steps ){
	/*if(!isset($position) && !isset($steps)){
		$position = "2";
		$steps = "4";
	}*/
	echo('<ul>');
	for($i=1; $i<=$steps; $i++){
		if($i <= $position){
			echo('<li class="active">'.$i.'</li>');
		} else {
			echo('<li>'.$i.'</li>');
		}
	}
	echo('</ul>');
}
?>