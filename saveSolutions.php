<?php
session_set_cookie_params ( 0 );
session_start ();
require_once ("config/config.php");
require_once ("functions/functions.php");

// Header
include ("includes/vheader.php");

$results = $_POST;
$id = $results['id'];
$rating = $results['rating'];
unset($results['id']);
unset($results['rating']);

$count = count( $results ) - 2;

$values = array_slice ( $results, 0, $count );

foreach ( $results as $result ) {
	$values[] = $result;
}
$i = 0;

// If we're rating lets add/substract
if($rating == 'true') {
	foreach ($results as $key => $value) {
		$key = str_replace("_"," ",$key);
		if($value == 'plus'){
			plusOne ( $db, $key );
		} else if($value == 'minus'){
				minusOne ( $db, $key );
		}
	}
} else {
	// We're adding solutions 
	$values = array_slice ( $_POST, 0, count($_POST)-1 );
	foreach($values as $value){
		saveSolution($db, $id, $value);
	}
}

	echo '<p class="megaSizeMe">Thank you for your contribution!<p>';
	echo '<p class="mediumSizeMe">We'."'".'ll be in touch!<p>';

include ("includes/vfooter.php");
?>



