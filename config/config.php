<?php
define('SITE_ROOT', 'http://www.planly.eu/');
ini_set('display_errors', 'On');
error_reporting(E_ALL);

// 0. DB parameters, not valid for SQLite usage, --> replace when using MySQL
$host = '';
$dbname = '';
$user = '';
$pass = '';

try {
	// DB from PDO object, using SQLite file from folder
	$db = new PDO('sqlite:db/planly.sqlite');
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	// Below how to create a SQLite DB
	//$DB->exec("CREATE TABLE projects (Id INTEGER PRIMARY KEY, name TEXT, email TEXT)");	
	
	// Uncomment this when using MySQL 
	//$DBH->exec("SET NAMES utf8;");
}
catch(PDOException $e) {
	echo("<h1>DB connection NOK</h1>");
	file_put_contents('log/DBErrors.txt', "DB connection error: " . $e->getMessage() . "\n", FILE_APPEND);
}


?>