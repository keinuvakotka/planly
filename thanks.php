<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

/**
 *  Handle which includes/v*-php to show
 */

//Header
include("includes/vheader.php");

if(isset($_POST['email']) && isset($_POST['problem'])){
	$email = $_POST['email'];
	$problem = $_POST['problem'];
	$emails = explode(",", $email);
	
	//print_r($emails) .'<br>';
	$id = getProjectIdWithProblem($db, $problem);
	$project = getProject($db, $id);
	if($id != false){
		saveEmails($db, $id, $emails);
		sendConfimation($db, $id);
		sendInvitation($db, $id);
		include("includes/vinviteconfirmation.php");
	}

} elseif (isset($_POST['message'])){
		$datas = $_POST;
		$datas['done'] = '';		
		$str = '';
		$id = $_SESSION['id'];
		foreach ($datas as $data){
			$str .= $data .'<br>';
		}
		sendResults($db, $id, $str);
		
		echo "<h1>Thank you, your project is now ready</h1>";
		echo '<p class="mediumSizeMe">The results were successfully sent to your project members.</p>';
		
}else {
	echo "Seems like you've accidentally stumbled here or something wen't horribly wrong.";
}

include("includes/vfooter.php");
?>



