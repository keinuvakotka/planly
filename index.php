<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

/**
 *  Handle which includes/v*-php to show
 */

//Header
include("includes/vheader.php");

//Index.php - vindex.php
if(empty($_POST)){
	include("includes/vindex.php");
} 

//We have problem, lets invite people - vinvitepeople.php
else if(isset($_POST['problem'])){
	$_SESSION['problem'] = $_POST['problem'];
	//saveProject($db, $_GET['problem'],'a@b.com');
	//ToDo: Handle different steps with corresponding template files
	include("includes/vcreateproject.php");
}

if(isset($_POST['mode']) && $_POST['mode'] == 'delete'){
	$id = $_POST['id'];
	deleteProject($db, $id);
	include("includes/vindex.php");
}

include("includes/vfooter.php");
?>



