<?php
session_set_cookie_params (0);
session_start();
require_once("config/config.php");
require_once("functions/functions.php");

//Header
include("includes/vheader.php");

if(isset($_GET['admin'])){
	$code = $_GET['admin'];
} else {
	header('Location: /planly');
}

if(isset($_POST['mode'])){
	$mode = $_POST['mode'];
} else {
	$mode = 'manage';
}

$id = getProjectID($db, $code, true);

if(isset($id)){
	$project=getProject($db, $id);
	if(is_object($project)){
		if($mode == 'edit'){
			include("includes/veditproject.php");
		} elseif($mode == 'start-rating'){
			startRating($db, $id);
			$_POST['edit'] = '';
			include("includes/vmanageproject.php");
		} else {
			if(isset($_POST['edit'])){
				$data = $_POST;
				$name = $data['problem'];
				$email = $project->email;
				$solutionDL = $data['solutionDL'];
				$ratingDL = $data['ratingDL'];
					
				if(empty($name)){$name = $project->name;}
				if(empty($solutionDL)){$solutionDL = $project->solutionDL;}
				if(empty($ratingDL)){$ratingDL = $project->ratingDL;}
					
				updateProject($db, $id, $name, $email, $solutionDL, $ratingDL);
				$_POST['edit'] = '';
			}

			include("includes/vmanageproject.php");
		}
	}
} else {
	echo '<p class="megaSizeMe">Not a valid code<p>';
	echo '<a href="#" onclick="showDialog()" class="code focus">Try again?</a>';
}

include("includes/vfooter.php");
?>