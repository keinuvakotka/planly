<?php
session_set_cookie_params (0);
session_start();
require_once("functions/functions.php");
require_once("config/config.php");
$radioid = 0;

if(isset($_POST['selected'])){
	$selected = $_POST['selected'];
	$id = $_POST['id'];
} else {
	header('Location: /planly');
}

//Header
include("includes/vheader.php");

if(isset($id)){
	$project=getProject($db, $id);
	$emails = getEmails($db, $id);
	include("includes/vworkdivision.php");
} else {
	echo '<p class="mediumSizeMe">Something went wrong, please try again.<p>';
}

//Footer
include("includes/vfooter.php");
?>



