<?php
/**
 * Function returns name for integer $id
 * @param $db
 * @return $return <string, name>
 */
function getProject($db, $id){
	$stmt = $db->prepare("SELECT * FROM projects WHERE Id=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	
	$result = $stmt->fetch(PDO::FETCH_OBJ);
	
  	return $result;
}

function getAllProjects($db){
	$stmt = $db->prepare("SELECT * FROM projects");
	$stmt->execute();

	$result = $stmt->fetchAll(PDO::FETCH_OBJ);

	return $result;
}

function getProjectByCipher($db, $cipher, $admin){
	if($admin){
		$stmt = $db->prepare("SELECT * FROM projects WHERE admincode=:cipher");
	} else {
		$stmt = $db->prepare("SELECT * FROM projects WHERE code=:cipher");
	}
	$stmt->bindParam(':cipher', $cipher);
	$stmt->execute();

	$result = $stmt->fetch(PDO::FETCH_OBJ);

	return $result;
}

function existsInDB($db, $cipher, $admin){
	if($admin){
		$stmt = $db->prepare("SELECT * FROM projects WHERE admincode=:cipher");
	} else {
		$stmt = $db->prepare("SELECT * FROM projects WHERE code=:cipher");
	}
	$stmt->bindParam(':cipher', $cipher);
	$stmt->execute();

	$result = $stmt->fetch(PDO::FETCH_OBJ);
	
	if(is_object($result)){
		return true;
	} else {
		return false;
	}
}



function getProjectID($db, $code, $admin){
	// Returns project id if (admin)code matches database or false
	if($admin){
		$stmt = $db->prepare("SELECT * FROM projects WHERE admincode=:code");
	} else {
		$stmt = $db->prepare("SELECT * FROM projects WHERE code=:code");
	}
	$stmt->bindParam(':code', $code);
	$stmt->execute();

	$result = $stmt->fetch(PDO::FETCH_OBJ);

	if(is_object($result)){
		return $result->Id;
	} else {
		return false;
	}
}

function getProjectIdWithProblem($db, $problem){
	// Returns project id if code matches database or false
	$stmt = $db->prepare("SELECT * FROM projects WHERE name=:problem");
	$stmt->bindParam(':problem', $problem);
	$stmt->execute();

	$result = $stmt->fetch(PDO::FETCH_OBJ);

	if(is_object($result)){
		return $result->Id;
	} else {
		return false;
	}
}

function getSolutions($db, $id) {
	
	$stmt = $db->prepare("SELECT * FROM pSolutions WHERE sID=:id ORDER BY sValue DESC");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	
  	return $result;
}

function getEmails($db, $id) {

	$stmt = $db->prepare("SELECT * FROM recipients WHERE rID=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	$result = $stmt->fetchAll(PDO::FETCH_OBJ);

	return $result;
}

function validateData($email, $contact, $user_email) {
	//validate if data exists
	if (isset($email) && isset($contact) && isset($user_email)){
		return true;
	}else {
		return false;
	}
}

function sendEmail($to, $subject, $message){
	//The structure of the email-message
	$message = wordwrap($message,80);
	$email_from = "Planly";
	//$headers = 'From: '.$email_from ."\r\n" . 'X-Mailer: PHP/' . phpversion();
	
	$headers = "From: noreply@planly.eu\r\n";
	$headers .= "Reply-To: lauri@planly.eu\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		
	/*$emails = explode(",", $to);
	
	foreach($emails as $email){
		$email = trim($email);
		if (!preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])*(\.([a-z0-9])([-a-z0-9_-])([a-z0-9])+)*$/i', $email)){
			// MAIL AWAY!
			mail($email, $subject, $message, $headers);
		}
	}*/
	
	$to = trim($to);
	if (!preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])*(\.([a-z0-9])([-a-z0-9_-])([a-z0-9])+)*$/i', $to)){
		mail($to, $subject, $message, $headers);
	}
}

function sendInvitation($db, $id){
	
	$subject = "New invitation from Planly to collaborate";
	
	$emails = getEmails($db, $id);
	$project = getProject($db, $id);
	
	$problem = $project->name;
	$solutionDL = $project->solutionDL;
	$code = $project->code;
	
	foreach ($emails as $email){
		
		$html= '<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head>';
		$html.='<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;">';
		$html.='<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr><td align="center" valign="top">';
		$html.='<table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;">';
		$html.='<tr><td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e">';
		$html.='<tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">Invitation to collaborate</h1>';
		$html.='</td></tr></table><!-- End Header--></td></tr><tr><td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600">';
		$html.='<tr><td valign="top" style="background-color: #fdfdfd;"><!-- Content --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top">';
		$html.='<div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '. $email->email . ", you've been invited to provide your ideas on";
		$html.='<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'. $problem .'</h2>';
		$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Provide your input at</h3>';
		$html.='<p><a href="http://www.planly.eu/contribute/'.$code.'" style="color:#d6336e;"><strong>planly.eu/contribute/'.$code.'</strong></a></p><table cellspacing="0" cellpadding="0" style="width: 100%;vertical-align: top;" border="0">';
		$html.='<tr><td valign="top"><h3 style="color:#505050; display:block; font-family:Arial;font-size:20px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Please contribute by</h3>';
		$html.='<p>'.$solutionDL.'</p></td></tr></table></div></td></tr></table><!--End Content --></td></tr></table><!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer -->';
		$html.='<table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
		$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p></td>';
		$html.='</tr></table></td></tr></table><!-- End Footer --></td></tr></table><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;">';
		$html.='<tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;">';
		$html.="<p>Don't want these messages anymore? " . '<strong><a href="http://www.planly.eu/unsubscribe/'.$email->email.'" style="color:#9495a4">unsubscribe</a></strong></p></td></tr></table></td></tr></table></div></body></html>';
		
		sendEmail($email->email, $subject, $html);
	}
}

function sendConfimation($db, $id){

	$subject = "Your new project on Planly";

	$project = getProject($db, $id);
	$email = $project->email;

	$problem = $project->name;
	$solutionDL = $project->solutionDL;
	$ratingDL = $project->ratingDL;
	$code = $project->code;
	$adminCode = $project->admincode;

	$html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;">';
	$html.='<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;">';
	$html.='<tr><td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e"><tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">Manage your project</h1>';
	$html.='</td></tr></table><!-- End Header--></td></tr><tr><td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600"><tr><td valign="top" style="background-color: #fdfdfd;"><!-- Content -->';
	$html.='<table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '.$email.', you'."'".'ve created project<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'.$problem.'</h2>';
	$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Follow this link to project management</h3><p><a href="http://www.planly.eu/manage/'.$adminCode.'" style="color:#d6336e;"><strong>planly.eu/manage/'.$adminCode.'</strong></a></p>';
	$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Or provide your own feedback here</h3><p><a href="http://www.planly.eu/contribute/'.$code.'" style="color:#d6336e;"><strong>planly.eu/contribute/'.$code.'</strong></a></p><table cellspacing="0" cellpadding="0" style="width: 100%;vertical-align: top;" border="0">';
	$html.='<tr><td valign="top"><h3 style="color:#505050; display:block; font-family:Arial;font-size:20px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Project timing</h3><p>Solutions by '.$solutionDL.'<br>Ratings by '.$ratingDL.'</p></td>';
	$html.='</tr></table></div></td></tr></table><!--End Content --></td></tr></table><!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer --><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
	$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p></td></tr></table>';
	$html.='</td></tr></table><!-- End Footer --></td></tr></table><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
	$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Don'."'".'t want these messages anymore? <strong><a href="http://www.planly.eu/unsubscribe/'.$email.'" style="color:#9495a4">unsubscribe</a></strong></p>';
	$html.='</td></tr></table></td></tr></table></td></tr></table></div></body></html>';

	sendEmail($email, $subject, $html);
}

function sendRatingInvitation($db, $id){

	$subject = "Planly needs your feedback";

	$emails = getEmails($db, $id);
	$project = getProject($db, $id);

	$problem = $project->name;
	$ratingDL = $project->ratingDL;
	$code = $project->code;

	foreach ($emails as $email){

		$html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">';
		$html.='<div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr><td align="center" valign="top">';
		$html.='<table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;"><tr>';
		$html.='<td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e">';
		$html.='<tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">Invitation to provide feedback</h1>';
		$html.='</td></tr></table><!-- End Header--></td></tr><tr><td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600"><tr>';
		$html.='<td valign="top" style="background-color: #fdfdfd;"><!-- Content --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top">';
		$html.='<div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '.$email->email.','." it's now time to rate ideas for";
		$html.='<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'.$problem.'</h2>';
		$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Follow the link</h3>';
		$html.='<p><a href="http://www.planly.eu/contribute/'.$code.'" style="color:#d6336e;"><strong>planly.eu/contribute/'.$code.'</strong></a></p><table cellspacing="0" cellpadding="0" style="width: 100%;vertical-align: top;" border="0">';
		$html.='<tr><td valign="top"><h3 style="color:#505050; display:block; font-family:Arial;font-size:20px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">to  rate solutions by</h3>';
		$html.='<p>'.$ratingDL.'</p></td></tr></table></div></td></tr></table><!--End Content --></td></tr></table><!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer -->';
		$html.='<table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
		$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p></td></tr></table></td></tr></table><!-- End Footer --></td></tr></table>';
		$html.='<table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
		$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>'."Don't".' want these messages anymore? <strong><a href="http://www.planly.eu/unsubscribe/'.$email->email.'" style="color:#9495a4">unsubscribe</a></strong></p>';
		$html.='</td></tr></table></td></tr></table></td></tr></table></div></body></html>';

		sendEmail($email->email, $subject, $html);
	}
	
	sendEmail($project->email, $subject, $html);
}

function sendInvitationToPickSolution($db, $id){

	$subject = "It's time to choose your solution!";

	$project = getProject($db, $id);
	
	$problem = $project->name;
	$email = $project->email;
	$adminCode = $project->admincode;

	$html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">';
	$html.='<div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">';
	$html.='<tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;">';
	$html.='<tr><td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e">';
	$html.='<tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">The solutions are in!</h1>';
	$html.='</td></tr></table><!-- End Header--></td></tr><tr><td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600">';
	$html.='<tr><td valign="top" style="background-color: #fdfdfd;"><!-- Content --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '.$email.', '."it's".' now time to pick a solution for';
	$html.='<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'.$problem.'</h2>';
	$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Follow the link to pick your favourite</h3>';
	$html.='<p><a href="http://www.planly.eu/pick-solution/'.$adminCode.'" style="color:#d6336e;"><strong>planly.eu/pick-solution/'.$adminCode.'</strong></a></p></div></td></tr></table><!--End Content --></td></tr></table>';
	$html.='<!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer --><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top">';
	$html.='<table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p>';
	$html.='</td></tr></table></td></tr></table><!-- End Footer --></td></tr></table><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr>';
	$html.='<td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;">';
	$html.="<p>Don't".' want these messages anymore? <strong><a href="http://www.planly.eu/unsubscribe/'.$email.'" style="color:#9495a4">unsubscribe</a></strong></p></td>';
	$html.='</tr></table></td></tr></table></td></tr></table></div></body></html>';

	sendEmail($email, $subject, $html);
	
}

function sendResults($db, $id, $message){

	$subject = "Results for your project on Planly";

	$project = getProject($db, $id);
	$emails = getEmails($db, $id);

	$problem = $project->name;
	
	foreach ($emails as $email){

		$html='<!DOCTYPE html><html><head><meta http-equiv="Content-Type" content="text/html;charset=UTF-8"><title>Planly.eu</title></head><body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"><div style="background-color: #f5f5f5;width:100%;-webkit-text-size-adjust:none !important;margin:0;padding: 70px 0 70px 0;"><table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"><tr>';
		$html.='<td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" width="600" style="-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;background-color: #fdfdfd;border: 1px solid #dcdcdc;"><tr><td align="center" valign="top"><!-- Header --><table border="0" cellpadding="0" cellspacing="0" width="600" style="background-color: #d6336e;color: #ffffff;border-bottom: 0;font-family:Arial;font-weight:bold;line-height:100%;vertical-align:middle;" bgcolor="#d6336e">';
		$html.='<tr><td><h1 style="color: #ffffff;margin:0;padding: 28px 24px;text-shadow: 0 1px 0 #707185;display:block;font-family:Arial;font-size:30px;font-weight:bold;text-align:left;line-height: 150%;">Results are in!</h1></td></tr></table><!-- End Header--></td></tr><tr>';
		$html.='<td align="center" valign="top"><!-- Body --><table border="0" cellpadding="0" cellspacing="0" width="600"><tr><td valign="top" style="background-color: #fdfdfd;"><!-- Content --><table border="0" cellpadding="20" cellspacing="0" width="100%"><tr><td valign="top"><div style="color: #737373;font-family:Arial;font-size:14px;line-height:150%;text-align:left;"> Hello '.$email->email.', here are your results for project<h2 style="color:#505050; display:block; font-family:Arial;font-size:30px; font-weight:bold; margin-top:10px; margin-right:0;margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">'.$problem.'</h2>';
		$html.='<h3 style="color:#505050; display:block;font-family:Arial; font-size:20px; font-weight:bold; margin-top:10px;margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;line-height:150%">Result and work division is as follows</h3><p>'.$message.'</p><table cellspacing="0" cellpadding="0" style="width: 100%;vertical-align: top;" border="0"><tr><td valign="top"></td></tr></table></div></td>';
		$html.='</tr></table><!--End Content --></td></tr></table><!-- End Body--></td></tr><tr><td align="center" valign="top"><!-- Footer --><table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>';
		$html.='<td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;"><p>Planly | Project planning made easy</p></td></tr></table></td></tr></table><!-- End Footer --></td></tr></table>';
		$html.='<table border="0" cellpadding="10" cellspacing="0" width="600" style="border-top:0;"><tr><td valign="top"><table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td colspan="2" valign="middle" id="credit" style="border:0;color: #9495a4;font-family: Arial;font-size:12px;line-height:125%;text-align:center;">';
		$html.="<p>Don'".'t want these messages anymore? <strong><a href="http://www.planly.eu/unsubscribe/'.$email->email.'" style="color:#9495a4">unsubscribe</a></strong></p></td></tr></table></td></tr></table></td></tr></table></div></body></html>';
		
		sendEmail($email->email, $subject, $html);
	}
	
	sendEmail($project->email, $subject, $html);
}

// The redirect function redirects to index.php so that the url is cleared of parametres
// WARNING! DEPRECATED!
function redirect(){
	//if URL contains 'index.php*' then cut the URL starting from index.php
	$url = "http://" . $_SERVER ['HTTP_HOST'] .$_SERVER['REQUEST_URI'];
	if (strrpos($url,'planly') !== false) {
		//unset($_GET['email']);
		$position = strrpos($url,"planly") +7;
		$url = substr($url, 0, $position);
		$url .= "thanks.php";
	}

	header('Location: '.$url);
}

function saveProject($db, $project, $email, $solutionDL, $ratingDL){
	// Inserting data into database
	$name = $project;
	$code = createCode($name); 
	while(existsInDB($db, $code, false)){
		// Atte provided annotation string to modify until code is unique
		$name .= 'mato';
		$code = createCode($name);
	}
	$str = $code . $email;
	$adminCode = createCode($str);
	while(existsInDB($db, $adminCode, true)){
		// Atte provided annotation string to modify until adminCode is unique
		$adminCode .= 'katkarapu';
		$adminCode = createCode($adminCode);
	}
	$stmt = $db->prepare("INSERT INTO projects VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
	$data = array(null, $project, $email, $code, $adminCode, $solutionDL, $ratingDL, 'false', 'false');
	$stmt->execute($data);
}

function updateProject($db, $id, $name, $email, $solutionDL, $ratingDL){

	$project = getProject($db, $id);	
	
	$stmt = $db->prepare("UPDATE projects SET Id=:id, name=:name, solutionDL=:solutionDL, ratingDL=:ratingDL WHERE Id=:PID");
	$conf = array(':id' => $id, ':name' => $name, ':solutionDL' => $solutionDL, ':ratingDL' => $ratingDL, ':PID' => $id);
	$stmt->execute($conf);
}

function startRating($db, $id){

	$stmt = $db->prepare("UPDATE projects SET rating='true' WHERE Id=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	
	sendRatingInvitation($db, $id);
}

function closeProject($db, $id){

	$stmt = $db->prepare("UPDATE projects SET closed='true' WHERE Id=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	
	sendInvitationToPickSolution($db, $id);

}

function saveSolution($db, $id, $solution){
	// Inserting data into database
	$stmt = $db->prepare("INSERT INTO pSolutions VALUES (?, ?, ?)");
	$data = array($id, $solution, 0);
	$stmt->execute($data);
}

function deleteProject($db, $id){
	$stmt = $db->prepare("DELETE FROM projects WHERE id=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();
	
	deleteSolutions($db, $id);
}

function deleteSolutions($db, $id){
	$stmt = $db->prepare("DELETE FROM pSolutions WHERE sID=:id");
	$stmt->bindParam(':id', $id);
	$stmt->execute();

	deleteSolutions($db, $id);
}

function saveEmails($db, $id, $emails){
	// Inserting data into database
	if(count($emails)>1){
		foreach ($emails as $email){
			$email = trim($email);
			$stmt = $db->prepare("INSERT INTO recipients VALUES (?, ?)");
			$data = array($id, $email);
			$stmt->execute($data);
		}
	} else {
		$emails = trim($emails[0]);
		$stmt = $db->prepare("INSERT INTO recipients VALUES (?, ?)");
		$data = array($id, $emails);
		$stmt->execute($data);
	}
	
}

function unSubscribe($db, $email){
	$stmt = $db->prepare("DELETE FROM recipients WHERE email=:email");
	$stmt->bindParam(':email', $email);
	$stmt->execute();
}

function createCode($string){
	// Creates sha1 hash from given string that is then cut to length
	$cipher = sha1($string);
		
	return substr($cipher, 0, 8);
}

function checkCode($db, $string){
	//Check that code matches something in db, (now) returns boolean
	$cipher = createCode($string);
	
	$data = getProjectByCipher($db, $cipher);
	
	if(is_object($data)){
		if ($cipher === $data->code){
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
	
}

function plusOne($db, $solution) {
	$stmt = $db->prepare("UPDATE pSolutions SET sValue = sValue + 1 WHERE solution =:solution");
	$stmt->bindParam(':solution', $solution);
	$stmt->execute();
}

function minusOne($db, $solution) {
	$stmt = $db->prepare("UPDATE pSolutions SET sValue = sValue - 1 WHERE solution =:solution");
	$stmt->bindParam(':solution', $solution);
	$stmt->execute();
}

function visualiseProgress($position, $steps ){
	$html = '<ul class="progressbar">';
	for($i=1; $i<=$steps; $i++){
		if($i < $position){
			$html.='<li class="step">'.$i.'</li>';
		} elseif ($i == $position){
			$html.='<li class="step last-active">'.$i.'</li>';
		} else {
			$html.='<li class="step">'.$i.'</li>';
		}
	}
	$html.='</ul>';
	
	return $html;
}

?>